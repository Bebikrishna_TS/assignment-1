import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Dbconnection {
    public static Connection getConnection() {
        Connection connection = null;
        String url = "jdbc:mysql://localhost:3306/mobile";
        String user = "bebikrishna";
        String password = "1234";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
             //System.out.println("driver loaded sucessfully");
            connection = DriverManager.getConnection(url, user, password);
           /* if(connection!=null) {
                ystem.out.println("connection done");
            }*/
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        return connection;
    }
    public static void closeConnection(Connection connection){
        if(connection!=null){
            try{
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

}

