import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UserDetailsDAO {
    public static UserDetails checkMobile(String userPhone) {
        Connection connection = Dbconnection.getConnection();
        UserDetails userDetails = null;
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from userDetails where Mobile=" + userPhone);
            while (resultSet.next()) {
                userDetails = new UserDetails();
                userDetails.setName(resultSet.getString(2));
                userDetails.setMobileNo(resultSet.getString(3));
                userDetails.setPassword(resultSet.getString(4));
                userDetails.setDob(resultSet.getString(5));
                userDetails.setGender(resultSet.getInt(6));
                userDetails.setAdress(resultSet.getString(7));
                userDetails.setPin(resultSet.getInt(8));
                userDetails.setStatus(resultSet.getInt(9));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            Dbconnection.closeConnection(connection);
        }
        return userDetails;

    }


}