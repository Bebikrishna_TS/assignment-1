import com.mysql.cj.protocol.Resultset;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Scanner;
import java.util.Date;
public class PhoneUser {
    public static PhoneUser phoneUser;
    private Scanner inputScanner = null;

    public static void main(String[] args) {
        phoneUser = new PhoneUser();
        allInformation();
    }
    public static void allInformation() {
        phoneUser.inputScanner = new Scanner(System.in);
        UserDetails selectedData = null;
        System.out.println("enter the phone no of user");
        String userPhoneNo = phoneUser.inputScanner.nextLine();
        selectedData = UserDetailsDAO.checkMobile(userPhoneNo);
        boolean isCardAvaillable = false;
        if (selectedData != null) {
            isCardAvaillable = true;
            if (userPhoneNo.length() == 10) {

                System.out.println("Enter password");
                String passward = phoneUser.inputScanner.nextLine();

                if (selectedData.getPassword().equals(passward)) {
                    int statusCode = selectedData.getStatus();
                    if (statusCode == 3) {
                        System.out.println("Licences expiry");
                    } else if (statusCode == 2) {
                        System.out.println("Application Acsess Blocked");
                    } else if (statusCode == 1) {
                        displayData(selectedData);
                    }
                } else {
                    System.out.println("wrong password");
                }
            } else {
                System.out.println(" phone number doesn't exsit");
            }
        } else {
            System.out.println("please enter valid phone no");
        }

    }

    public static void displayData(UserDetails userDetails) {
        System.out.println("User Details Output:");

        System.out.println("*************" + userDetails.getName().toUpperCase() + "****************");
        System.out.println("**********************************************");
        System.out.println("Mobile : +91-" + userDetails.getMobileNo());
        String displayDate = "";
        String result = userDetails.getDob();

        try {
            Date date1 = new SimpleDateFormat("dd-MM-yyyy").parse(result);
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
            displayDate = dateFormat.format(date1);
            System.out.println(displayDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int code = userDetails.getGender();
        if (code == 2) {
            System.out.println("Gender : Female");
        } else if (code == 1) {
            System.out.println("Gender : Male");
        }
        System.out.println(userDetails.getAdress() + "," + userDetails.getPin());
        System.out.println("*****************************************");
        System.out.println("******************************************");

    }

}




